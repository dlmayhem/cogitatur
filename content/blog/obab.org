#+title: On Being and Becoming
#+author: Orion
#+date: 2020-07-14
#+tags[]: orion metal música


{{< youtube l9Yrw_1vkDg >}}

* On Being and Becoming

Faceless King before the time, \\
In your frozen throne beyond any place. \\
Thou who confer meaning from chaos, \\
Show me the secrets of what always remains. 

How could my words return to Thee? \\
For the echo of my voice will be dead at dawn, \\
And my troubled eyes can't defeat the mist, \\
So how could I walk through the eternal lawns?

A perfect model like no other can be, \\
Strictly crafted by the hands of doom.

Majestic the realm of the undying beliefs. \\
Of what always is and never becomes. \\
But perceptual fields are confusing and weak, \\
Where everything is drawn in upsetting tones.

Only by force such essences could \\
Be unified in one only being. 

Not needed of eyes when there's nothing to see, \\
And the absence of sound does not precise ears. \\
No arms, no legs, cause there's nothing to grip, \\
Turned to itself over years and years.

Time there was and there will be, \\
Every experience and every deed, \\
Pale reflections of the Highest Soul. \\
That is the nature of what can be told. 
