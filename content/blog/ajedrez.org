#+title: Conceptos básicos de ajedrez
#+author: cloyolam - dlmayhem
#+date: 2021-07-20 
#+tags[]: ajedrez taller

*NOTA:* Reposteo esto a un año del pequeño taller que hicimos con mi
hermano para celebrar el [[https://es.wikipedia.org/wiki/D%C3%ADa_internacional_del_ajedrez][día internacional del ajedrez]].

* Conceptos básicos de ajedrez

Algunos apuntes del mini taller que realizamos para los amigos en el
día mundial del ajedrez.

** El tablero

*** Partes 
- Consta de 64 casillas o escaques en disposición de 8x8.
- Se distribuyen casillas claras y oscuras de forma intercalada.
- Cada jugador se sitúa en lados opuestos del tablero, procurando que
  la primera casilla a su izquierda sea de color oscuro.
- *Fila:* Cada una de las líneas horizontales.
- *Columna:* Cada una de las líneas verticales.
- *Diagonales:* Líneas de casillas del mismo color.
- *Flancos:* 	     
  - *De Rey*: mitad del tablero del lado del Rey.
  - *De Dama*: mitad del tablero del lado de la Dama.

*** Coordenadas
- *Se asignan coordenadas al tablero:*
  - *Columnas:* letras /a-h/.
  - *Filas:* números /1-8/.

{{< figure
src="https://gitlab.com/dlmayhem/cogitatur/-/raw/master/static/coord.png"
>}}
 
** Las piezas

*** Nombre y valor relativo
| SÍMBOLO | PIEZA   | VALOR |
|---------+---------+-------|
| ♔       | Rey     |     ∅ |
| ♕       | Dama    |     9 |
| ♖       | Torre   |     5 |
| ♗       | Alfil   |     3 |
| ♘       | Caballo |     3 |
| ♙       | Peón    |     1 |

*** Orden
{{< figure
src="https://gitlab.com/dlmayhem/cogitatur/-/raw/master/static/orden.png"
>}}

*** Movimientos
- *♔ Rey* :: Se puede mover en cualquier dirección, pero solamente una
         casilla a la vez (excepto en el enroque).
- *♕ Dama* :: Puede moverse la cantidad de casillas que desee por filas,
          columnas y diagonales.
- *♖ Torre* :: Puede moverse la cantidad de casillas que desee
           por filas y columnas.
- *♗ Alfil* :: Son dos, uno blanco y uno negro. Puede moverse la cantidad
           de casillas que desee por las diagonales que sean de su
           color. 
- *♘ Caballo* :: Puede moverse en cualquier dirección formando una /L/ en
             un intervalo de 4 casillas. Puede pasar por encima de
             otras piezas.
- *♙ Peón* :: Puede moverse sólo hacia adelante y una casilla a la
          vez. Sólo en su primer movimiento tiene permitido avanzar
          dos veces. Captura en diagonal. 

** Movimientos especiales

*** Enroque
- *Se realiza con el Rey y alguna torre:*
  - Se mueve el Rey dos casillas en dirección a la torre.
  - Luego se posiciona la torre en la casilla junto al Rey en el lado
    opuesto en el que se encontraba la torre.
- Ambas piezas no deben haber sido movidas con anterioridad.
- El Rey no puede estar ni pasar por una posición de jaque.

*** Captura al paso
- Consiste la captura de un peón que se ha movido dos casillas en su
  jugada inicial por parte de un peón enemigo.
- La jugada debe realizarse de forma inmediata luego del avance de la
  pieza a capturar.

*** Coronación
- Ocurre cuando un peón alcanza el extremo enemigo del tablero.
- En este momento, el jugador puede promover su peón a cualquier pieza
  mayor (caballo, alfil, torre o Dama).

** Notación

*** Algebraica
| ELEMENTO        | NOTACIÓN        | EJEMPLO |
|-----------------+-----------------+---------|
| *Casillas*      | (columna, fila) | g4      |
| *Piezas*        | R, D, T, A, C   | --      |
| *Enroque corto* | 0-0             | --      |
| *Enroque largo* | 0-0-0           | --      |
| *Captura*       | x               | Cxd5    |
| *Coronación*    | =               | a8=T    |
| *Jaque*         | +               | Dg5+    |
| *Jaque mate*    | ++              | g7++    |

- *Ejemplo de apertura:* \\
  1 e4      e5 \\
  2 Cf3     Cc6 \\
  3 Ab5     d6 \\
  4 0-0     Ad7 \\
  5 Axc6    Axc6 \\ 
  6 ...   
- *En caso de ambigüedad:* 
  - Indicar la columna de origen de la pieza que se moverá.
    *Ej.:* /Ccxd4/ indica que el caballo de la columna /c/ será el que
    capture la pieza en /d4/.
  - Si este paso falla, se realiza lo mismo pero con el número de
    columna original (sólo si el paso anterior no resuelve).

*** Descriptiva
- Ya no es considerada oficial por la [[https://www.fide.com/][FIDE]].
- Hay una gran cantidad de libros (sobre todo los más antiguos) que
  usan la notación descriptiva.
- Ver [[https://en.wikipedia.org/wiki/Descriptive_notation][Wikipedia]].

** Recursos[fn:1]

- [[https://lichess.org][lichess]] (completo servidor online)
- [[https://stockfishchess.org/][Stockfish]] (motor de ajedrez)

** Footnotes

[fn:1] Software libre.
